﻿using System.Diagnostics;

const int SIZE = 10000000;
var randomArray = new int[SIZE];
const int numberofthreads = 4;

var random = new Random();

for (int i = 0; i < SIZE; i++)
{
    randomArray[i] = random.Next(0, 100);
}

var summ = 0;
var stopwatch = new Stopwatch();
stopwatch.Start();
for (int i = 0; i < SIZE; i++)
{
    summ += randomArray[i];
}
stopwatch.Stop();
Console.WriteLine($"Summ {summ} ElapsedTime: {stopwatch.ElapsedMilliseconds}");
stopwatch.Reset();

stopwatch = new Stopwatch();
stopwatch.Start();
summ = randomArray.AsParallel().Sum();
stopwatch.Stop();
Console.WriteLine($"Summ {summ} ElapsedTime: {stopwatch.ElapsedMilliseconds}");
stopwatch.Reset();


var sum = new int[numberofthreads];
var threadsarray = new Thread[numberofthreads];
summ = 0;
stopwatch.Start();
for (var i = 0; i < numberofthreads; i++)
{
    var tmp = i;
    threadsarray[i] = new Thread(() => MyThreadMethod(tmp, randomArray, sum));
    threadsarray[i].Start();
}
for (int i = 0; i < numberofthreads; i++)
{
    threadsarray[i].Join();
}
for (int i = 0; i < numberofthreads; i++)
{
    summ += sum[i];
}

stopwatch.Stop();
Console.WriteLine($"Summ {summ} ElapsedTime: {stopwatch.ElapsedMilliseconds}");
stopwatch.Reset();

static void MyThreadMethod(int threadid, int[] array, int[] sum)
{
    int thid = threadid;
    for (int i = thid * (array.Length / numberofthreads); i < (thid + 1) * array.Length / numberofthreads; i++)
    {
        sum[thid] += array[i];
    }
}


